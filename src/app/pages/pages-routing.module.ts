import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'erpTimesheets',
    pathMatch: 'full'
  },
  {
    path: 'erpTimesheets',
    loadChildren: () => import('./components/erpTimesheets/erp-timesheets.module').then(m => m.ErpTimesheetsModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
