import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {EmployeeType} from '../_models/EmployeeType';
import {EmployeeTypeInputModel} from '../_models/EmployeeTypeInputModel';
import {EmployeeTypeViewModel} from '../_models/EmployeeTypeViewModel';
import {authorizationData} from '../../../../../../../credential';

@Injectable({
  providedIn: 'root'
})
export class EmployeeTypeService {

  private apiBaseUrl = 'http://localhost:9500/e-crystal/api/v1/' + 'enterprise' + '/employeetypes';
  private httpReadOptions = {headers: { Authorization : `Basic ${authorizationData}` }};
  private httpWriteOptions = {headers: { Authorization : `Basic ${authorizationData}`, 'Content-Type': 'application/json' }};

  displayMap: Map<string, Map<string, boolean>> = new Map<string, Map<string, boolean>>();

  fields = {
    employee: 'employee',
    name: 'name',
  };

  sortableFields = [
    this.fields.name,
  ];

  filterOptions = [
    this.fields.name,
  ];

  fetchItemConditions: EmployeeType = new EmployeeType();
  hasFilter: boolean;

  defaultFilterOptions: string[] = [
    this.fields.name,
  ];

  chosenFilterOptions: string[] = [
    this.fields.name,
  ];

  constructor(
    private http: HttpClient,
  ) { }

  // ----- READ ----- //

  getEmployeeTypes(): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched employeeTypes')),
        catchError(this.handleError('Get employee types', []))
      );
  }

  getEmployeeTypeByExternalId(selectedId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${selectedId}`;
    return this.http.get<EmployeeType>(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched employee type with external id = ' + selectedId)),
        catchError(this.handleError('Get employee type by external id'))
      );
  }

  getEmployeeTypesByPageUrl(url: string): Observable<any> {
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched employee types by page url')),
        catchError(this.handleError('Get employee types by page url', []))
      );
  }

  getEmployeeTypesWithFilter(): Observable<any> {
    const url = this.apiBaseUrl + '/filter?' + this.createQueryParameters(this.fetchItemConditions);

    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched employee types with filter')),
        catchError(this.handleError('Get employee types with filter'))
      );
  }
  // ----- CREATE ----- //

  createEmployeeType(employeeType: EmployeeTypeInputModel): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.post(url, employeeType, this.httpWriteOptions)
      .pipe(
        tap((newEmployeeType: EmployeeTypeViewModel) => {
          this.log('create new employee type with external id = ' + newEmployeeType.externalId);
        }),
        catchError(this.handleError('Get employee type by external id'))
      );
  }

  // ----- UPDATE ----- //

  saveEmployeeType(employeeType: EmployeeTypeInputModel): Observable<any> {
    const url = this.apiBaseUrl + `/${employeeType.externalId}`;
    return this.http.put(url, employeeType, this.httpWriteOptions)
      .pipe(
        tap((editedEmployeeType: EmployeeTypeViewModel) => {
          this.log('update employee type with external id = ' + editedEmployeeType.externalId);
        }),
        catchError(this.handleError('Update employee type'))
      );
  }

  // ----- DELETE ----- //

  delete(externalId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${externalId}`;
    return this.http.delete(url, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('delete employee type with external id = ' + externalId)),
        catchError(this.handleError('Delete employee type'))
      );
  }

  // ----- ERROR HANDLING ----- //

  private handleError<T>(operation = 'operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  // ----- LOGGING ----- //

  private log(message: string): void {
    console.log(`EmployeeTypeService: ${message}`);
  }

  // ----- MISC ----- //

  createQueryParameters(fetchItemConditions: EmployeeType): string {
    const parameters: string[] = [];

    if (fetchItemConditions.name) {
      parameters.push('name=' + fetchItemConditions.name);
    }

    if (fetchItemConditions.sortBy) {
      parameters.push('sortBy=' + fetchItemConditions.sortBy);
    }

    if (fetchItemConditions.isAscending !== null) {
      if (fetchItemConditions.isAscending) {
        parameters.push('isAscending=true');
      } else {
        parameters.push('isAscending=false');
      }
    }

    return parameters.join('&');
  }

  resetDefaultFilterOptions(): void {
    this.chosenFilterOptions = this.defaultFilterOptions;
  }

  resetFilterData(): void {
    this.filterOptions.forEach(option => {
      this.fetchItemConditions[option] = null;
    });
  }

  setHasFilter(): void {
    this.hasFilter = false;
    for (const option of this.filterOptions) {
      if (this.fetchItemConditions[option] !== null) {
        this.hasFilter = true;
      }
    }
  }

  /* ----- DISPLAY SUB ITEMS ----- */

  addItemsToDisplayMap(employeeTypes: EmployeeTypeViewModel[]): void {
    const value = new Map([
      [this.fields.employee, false],
    ]);
    employeeTypes.forEach(employeeType => {
      this.displayMap.set(employeeType.externalId, value);
    });
  }

  isAnySubitemsDisplayed(): boolean {
    for (const [, itemDisplayMap] of this.displayMap) {
      for (const [, isDisplay] of itemDisplayMap) {
        if (isDisplay) {
          return true;
        }
      }
    }
    return false;
  }

  isSubitemsDisplay(itemExternalId: string, fieldName: string): boolean {
    return this.displayMap.get(itemExternalId).get(fieldName) === true;
  }

  hideAllSubitemsDisplay(): void {
    const value = new Map([
      [this.fields.employee, false],
    ]);
    this.displayMap.forEach((oldValue, externalId) => {
      this.displayMap.set(externalId, value);
    });
  }

  toggleDisplaySubitem(itemExternalId: string, fieldName: string): void {
    if (this.displayMap.get(itemExternalId).get(fieldName)) {
      this.hideItem(itemExternalId, fieldName);
    } else {
      this.displayItem(itemExternalId, fieldName);
    }
  }

  displayItem(itemExternalId: string, fieldName: string): void {
    const value = new Map([
      [this.fields.employee, fieldName === this.fields.employee],
    ]);
    this.displayMap.set(itemExternalId, value);
  }

  hideItem(itemExternalId: string, fieldName: string): void {
    this.displayMap.get(itemExternalId).set(fieldName, false);
  }

}
