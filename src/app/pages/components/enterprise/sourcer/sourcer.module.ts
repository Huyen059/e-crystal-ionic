import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SourcerRoutingModule } from './sourcer-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SourcerRoutingModule
  ]
})
export class SourcerModule { }
