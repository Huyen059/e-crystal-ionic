import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Sourcer} from '../_models/Sourcer';
import {SourcerInputModel} from '../_models/SourcerInputModel';
import {SourcerViewModel} from '../_models/SourcerViewModel';
import {authorizationData} from '../../../../../../../credential';

@Injectable({
  providedIn: 'root'
})
export class SourcerService {

  private apiBaseUrl = 'http://localhost:9500/e-crystal/api/v1/' + 'enterprise' + '/sourcers';
  private httpReadOptions = {headers: { Authorization : `Basic ${authorizationData}` }};
  private httpWriteOptions = {headers: { Authorization : `Basic ${authorizationData}`, 'Content-Type': 'application/json' }};

  displayMap: Map<string, Map<string, boolean>> = new Map<string, Map<string, boolean>>();

  fields = {
    sourcerType: 'sourcer type',
    name: 'name',
    employee: 'employee'
  };

  sortableFields = [
    this.fields.name,
  ];

  filterOptions = [
    this.fields.sourcerType,
    this.fields.name,
  ];

  fetchItemConditions: Sourcer = new Sourcer();
  hasFilter: boolean;

  defaultFilterOptions: string[] = [
    this.fields.name,
    this.fields.sourcerType,
  ];

  chosenFilterOptions: string[] = [
    this.fields.name,
    this.fields.sourcerType,
  ];

  constructor(
    private http: HttpClient,
  ) { }

  // ----- READ ----- //

  getSourcers(): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched sourcers')),
        catchError(this.handleError('Get sourcers', []))
      );
  }

  getSourcerByExternalId(selectedId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${selectedId}`;
    return this.http.get<Sourcer>(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched sourcer with external id = ' + selectedId)),
        catchError(this.handleError('Get sourcer by external id'))
      );
  }

  getSourcersByPageUrl(url: string): Observable<any> {
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched sourcers by page url')),
        catchError(this.handleError('Get sourcers by page url', []))
      );
  }

  getSourcersWithFilter(): Observable<any> {
    const url = this.apiBaseUrl + '/filter?' + this.createQueryParameters(this.fetchItemConditions);

    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched sourcers with filter')),
        catchError(this.handleError('Get sourcers with filter'))
      );
  }

  // ----- CREATE ----- //

  createSourcer(sourcer: SourcerInputModel): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.post(url, sourcer, this.httpWriteOptions)
      .pipe(
        tap((newSourcer: SourcerViewModel) => this.log('create new sourcer with external id = ' + newSourcer.externalId)),
        catchError(this.handleError('Get sourcer by external id'))
      );
  }

  // ----- UPDATE ----- //

  saveSourcer(sourcer: SourcerInputModel): Observable<any> {
    const url = this.apiBaseUrl + `/${sourcer.externalId}`;
    return this.http.put(url, sourcer, this.httpWriteOptions)
      .pipe(
        tap((editedSourcer: SourcerViewModel) => this.log('update sourcer with external id = ' + editedSourcer.externalId)),
        catchError(this.handleError('Update sourcer'))
      );
  }

  // ----- DELETE ----- //

  delete(externalId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${externalId}`;
    return this.http.delete(url, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('delete sourcer with external id = ' + externalId)),
        catchError(this.handleError('Delete sourcer'))
      );
  }

  // ----- ERROR HANDLING ----- //

  private handleError<T>(operation = 'operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  // ----- LOGGING ----- //

  private log(message: string): void {
    console.log(`SourcerService: ${message}`);
  }

  // ----- MISC ----- //

  createQueryParameters(fetchItemConditions: Sourcer): string {
    const parameters: string[] = [];

    if (fetchItemConditions.name) {
      parameters.push('name=' + fetchItemConditions.name);
    }

    if (fetchItemConditions.sourcerType) {
      parameters.push('sourcerType=' + fetchItemConditions.sourcerType);
    }

    if (fetchItemConditions.sortBy) {
      parameters.push('sortBy=' + fetchItemConditions.sortBy);
    }

    if (fetchItemConditions.isAscending !== null) {
      if (fetchItemConditions.isAscending) {
        parameters.push('isAscending=true');
      } else {
        parameters.push('isAscending=false');
      }
    }

    return parameters.join('&');
  }

  resetDefaultFilterOptions(): void {
    this.chosenFilterOptions = this.defaultFilterOptions;
  }

  resetFilterData(): void {
    this.filterOptions.forEach(option => {
      this.fetchItemConditions[option] = null;
    });
  }

  setHasFilter(): void {
    this.hasFilter = false;
    for (const option of this.filterOptions) {
      if (this.fetchItemConditions[option] !== null) {
        this.hasFilter = true;
      }
    }
  }

  /* ----- DISPLAY SUB ITEMS ----- */

  addItemsToDisplayMap(sourcers: SourcerViewModel[]): void {
    const value = new Map([
      [this.fields.employee, false],
    ]);
    sourcers.forEach(sourcer => {
      this.displayMap.set(sourcer.externalId, value);
    });
  }

  isAnySubitemsDisplayed(): boolean {
    for (const [, itemDisplayMap] of this.displayMap) {
      for (const [, isDisplay] of itemDisplayMap) {
        if (isDisplay) {
          return true;
        }
      }
    }
    return false;
  }

  isSubitemsDisplay(itemExternalId: string, fieldName: string): boolean {
    return this.displayMap.get(itemExternalId).get(fieldName) === true;
  }

  hideAllSubitemsDisplay(): void {
    const value = new Map([
      [this.fields.employee, false],
    ]);
    this.displayMap.forEach((oldValue, externalId) => {
      this.displayMap.set(externalId, value);
    });
  }

  toggleDisplaySubitem(itemExternalId: string, fieldName: string): void {
    if (this.displayMap.get(itemExternalId).get(fieldName)) {
      this.hideItem(itemExternalId, fieldName);
    } else {
      this.displayItem(itemExternalId, fieldName);
    }
  }

  displayItem(itemExternalId: string, fieldName: string): void {
    const value = new Map([
      [this.fields.employee, fieldName === this.fields.employee],
    ]);
    this.displayMap.set(itemExternalId, value);
  }

  hideItem(itemExternalId: string, fieldName: string): void {
    this.displayMap.get(itemExternalId).set(fieldName, false);
  }

}
