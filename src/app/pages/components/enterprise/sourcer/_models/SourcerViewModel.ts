export class SourcerViewModel {
  externalId: string;
  name: string;
  sourcerType: {
    resourceUri: string,
    name: string,
    externalId: string
  };
}
