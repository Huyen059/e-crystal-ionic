export class SourcerInputModel {
  constructor(
    public externalId: string = null,
    public name: string = null,
    public sourcerType: string = null
  ) {  }
}
