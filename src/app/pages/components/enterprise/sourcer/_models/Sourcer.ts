export class Sourcer {
  constructor(
    public externalId: string = null,
    public name: string = null,
    public sourcerType: string = null,
    public sortBy: string = null,
    public isAscending: boolean = null,
  ) {  }
}
