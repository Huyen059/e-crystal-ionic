import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {EmployeeOutputModel} from '../_models/EmployeeOutputModel';
import {Employee} from '../_models/Employee';
import {EmployeeInputModel} from '../_models/EmployeeInputModel';
import {enterpriseProperties} from '../../../_properties/enterpriseProperties';
import {authorizationData} from '../../../../../../../credential';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private apiBaseUrl = enterpriseProperties.baseUrl + '/employees';
  private httpReadOptions = {headers: {Authorization: `Basic ${authorizationData}`}};
  private httpWriteOptions = {headers: {Authorization: `Basic ${authorizationData}`, 'Content-Type': 'application/json'}};

  fields = {
    active: 'active',
    contractType: 'contract type',
    email: 'email',
    employeeType: 'employee type',
    mobile: 'mobile',
    name: 'name',
    person: 'person',
    sourcer: 'sourcer',
    projectResource: 'project resource'
  };

  filterOptions = [
    this.fields.active,
    this.fields.contractType,
    this.fields.email,
    this.fields.employeeType,
    this.fields.mobile,
    this.fields.person,
    this.fields.sourcer
  ];

  defaultFilterOptions: string[] = [
    this.fields.employeeType,
  ];

  chosenFilterOptions: string[] = [
    this.fields.employeeType,
  ];

  fetchItemConditions: Employee = new Employee();
  private _hasFilter: boolean;

  constructor(
    private http: HttpClient,
  ) {  }

  /* ----- READ ----- */

  getEmployees(): Observable<any> {
    let url = this.apiBaseUrl;
    this.updateHasFilter();
    if (this.hasFilter) {
      url += '?' + this.createQueryParameters(this.fetchItemConditions);
    }
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched employees')),
        catchError(this.handleError('Get employees', []))
      );
  }

  getEmployeeByExternalId(selectedId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${selectedId}`;
    return this.http.get<EmployeeOutputModel>(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched employee with external id = ' + selectedId)),
        catchError(this.handleError('Get employee by external id'))
      );
  }

  getEmployeesByPageUrl(url: string): Observable<any> {
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched employees by page url')),
        catchError(this.handleError('Get employees by page url', []))
      );
  }

  search(searchBy: string, term: string): Observable<any> {
    if (searchBy === 'name') {
    this.fetchItemConditions.name = term.trim();
    }
    return this.getEmployees();
  }

  /* ----- CREATE ----- */

  createEmployee(employee: EmployeeInputModel): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.post(url, employee, this.httpWriteOptions)
      .pipe(
        tap((newEmployee: EmployeeOutputModel) => this.log('create new employee with external id = ' + newEmployee.externalId)),
        catchError(this.handleError('Get employee by external id'))
      );
  }

  /* ----- UPDATE ----- */

  saveEmployee(employee: EmployeeInputModel): Observable<any> {
    const url = this.apiBaseUrl + `/${employee.externalId}`;
    return this.http.put(url, employee, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('update employee')),
        catchError(this.handleError('Update employee'))
      );
  }

  /* ----- DELETE ----- */

  delete(externalId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${externalId}`;
    return this.http.delete(url, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('delete employee with external id = ' + externalId)),
        catchError(this.handleError('Delete employee'))
      );
  }

  /* ----- ERROR HANDLING ----- */

  private handleError<T>(operation = 'operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /* ----- LOGGING ----- */

  private log(message: string): void {
    console.log(`EmployeeService: ${message}`);
  }

  /* ----- FILTER ----- */

  createQueryParameters(fetchItemConditions: Employee): string {
    const parameters: string[] = [];

    if (fetchItemConditions.name) {
      parameters.push('name=' + fetchItemConditions.name);
    }

    if (fetchItemConditions.email) {
      parameters.push('email=' + fetchItemConditions.email);
    }

    if (fetchItemConditions.employeeType) {
      parameters.push('employeeType=' + fetchItemConditions.employeeType.externalId);
    }

    if (fetchItemConditions.contractType) {
      parameters.push('contractType=' + fetchItemConditions.contractType);
    }

    if (fetchItemConditions.mobile) {
      parameters.push('mobile=' + fetchItemConditions.mobile);
    }

    if (fetchItemConditions.person) {
      parameters.push('person=' + fetchItemConditions.person);
    }

    if (fetchItemConditions.sourcer) {
      parameters.push('sourcer=' + fetchItemConditions.sourcer.externalId);
    }

    if (fetchItemConditions.active) {
      parameters.push('active=' + fetchItemConditions.active);
    }

    if (fetchItemConditions.sortBy) {
      parameters.push('sortBy=' + fetchItemConditions.sortBy);
    }

    if (fetchItemConditions.isAscending !== null) {
      if (fetchItemConditions.isAscending) {
        parameters.push('isAscending=true');
      } else {
        parameters.push('isAscending=false');
      }
    }

    return parameters.join('&');
  }

  findIndexOfChosenFilterOption(fieldName: string): number {
    return this.chosenFilterOptions.findIndex(chosenOption => chosenOption === fieldName);
  }

  isFilterOptionChosen(fieldName: string): boolean {
    return this.findIndexOfChosenFilterOption(fieldName) !== -1;
  }

  resetDefaultFilterOptions(): void {
    this.chosenFilterOptions = this.defaultFilterOptions;
  }

  resetFilter(): void {
    this.filterOptions.forEach(option => {
      this.fetchItemConditions[option] = null;
    });
    this.resetDefaultFilterOptions();
    this._hasFilter = false;
  }

  resetFetchItemConditions(): void {
    this.fetchItemConditions = new Employee();
  }

  updateHasFilter(): void {
    this._hasFilter = false;
    for (const option of this.filterOptions) {
      if (this.fetchItemConditions[option] !== null) {
        this._hasFilter = true;
        break;
      }
    }
  }

  get hasFilter(): boolean {
    return this._hasFilter;
  }
}
