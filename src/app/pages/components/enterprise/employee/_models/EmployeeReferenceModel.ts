export class EmployeeReferenceModel {
  resourceUri: string;
  name: string;
  externalId: string;
}
