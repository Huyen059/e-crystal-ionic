export class EmployeeInputModel {
  constructor(
    public active: string = null,
    public contractType: string = null,
    public email: string = null,
    public employeeType: string = null,
    public externalId: string = null,
    public mobile: string = null,
    public name: string = null,
    public person: string = null,
    public sourcer: string = null,
  ) {  }
}
