import {EmployeeReferenceModel} from './EmployeeReferenceModel';
import {EmployeeOutputModel} from './EmployeeOutputModel';

export class EmployeeOutputMapper {
  static map(employeeReferenceModel: EmployeeReferenceModel): EmployeeOutputModel {
    const employeeOutputModel = new EmployeeOutputModel();
    employeeOutputModel.name = employeeReferenceModel.name;
    employeeOutputModel.externalId = employeeReferenceModel.externalId;
    return employeeOutputModel;
  }
}
