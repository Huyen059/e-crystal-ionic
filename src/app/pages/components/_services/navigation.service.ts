import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  parentElementFullName: string;
  parentExternalId: string;

  private router = new Subject<Router>();
  currentRouter = this.router.asObservable();

  private reloadTime = new Subject<Date>();
  reload = this.reloadTime.asObservable();

  constructor() { }

  changeRouter(router: Router): void {
    this.router.next(router);
  }

  changeReloadTime(time: Date): void {
    this.reloadTime.next(time);
  }

}
