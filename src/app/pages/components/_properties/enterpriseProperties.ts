import {appBaseUrl} from './appBaseUrl';

export const enterpriseProperties = {
  baseUrl: appBaseUrl + '/enterprise',
};
