import {appBaseUrl} from './appBaseUrl';

export const erpTimesheetsProperties = {
  baseUrl: appBaseUrl + '/erpTimesheets',
};

