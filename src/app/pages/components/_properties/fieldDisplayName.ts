export const fieldDisplayNames = {
  name: 'Name',
  employee: 'Employee',
  month: 'Month',
  year: 'Year',
  status: 'Status',
  totalHours: 'Total hours',
  totalManDays: 'Total man days',
  entries: 'Entries',
  remarks: 'Remarks',
};
