import { Component, OnInit } from '@angular/core';
import {TimesheetOutputModel} from '../_models/TimesheetOutputModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {ActivatedRoute, Router} from '@angular/router';
import {TimesheetService} from '../_services/timesheet.service';
import {AuthService} from '../../../../../login/_services/auth.service';
import {NavigationService} from '../../../_services/navigation.service';
import {User} from '../../../../../login/_models/User';
import {PaginatedResponse} from '../_models/PaginatedResponse';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-timesheet-list',
  templateUrl: './timesheet-list.component.html',
  styleUrls: ['./timesheet-list.component.scss'],
})
export class TimesheetListComponent implements OnInit {
  private elementFullName = 'erpTimesheets.timesheet';
  editButtonShown = false;
  actionButtonsShown = false;

  timesheets: TimesheetOutputModel[] = [];
  private links: LinksModel;
  page: PageModel;

  selectedItems = new Map<string, TimesheetOutputModel>();
  numberOfSelectedItems = new Subject<number>();
  displayActionButtons = this.numberOfSelectedItems.asObservable();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private timesheetService: TimesheetService,
    // private filterService: FilterService,
    private authService: AuthService,
    private navigationService: NavigationService,
  ) {
    this.displayActionButtons.subscribe(numberOfSelectedItems => {
      if (numberOfSelectedItems > 0) {
        this.actionButtonsShown = true;
        if (numberOfSelectedItems === 1) {
          this.editButtonShown = true;
        }
      }
    });
  }

  get currentUser(): User {
    return this.authService.currentUserValue;
  }

  get firstItemOfCurrentPage(): number {
    return (this.page.number - 1) * this.page.size + 1;
  }

  get lastItemOfCurrentPage(): number {
    if (this.page.number < this.page.totalPages) {
      return this.page.number * this.page.size;
    }

    return this.page.totalElements;
  }

  ngOnInit() {
    // this.filterService.resetFilterData();
    // this.filterService.addFilter(this.elementFullName);
    this.navigationService.changeRouter(this.router);
    this.getTimesheetOnInit();
  }

  private getTimesheetOnInit(): void {
    if (this.timesheetService.pageUrl) {
      this.getTimesheetsByPageUrl(this.timesheetService.pageUrl);
    } else {
      this.getTimesheets();
    }
  }

  /* ----- HTTP REQUESTS -----*/

  private getTimesheets(): void {
    this.timesheetService.getTimesheets()
      .subscribe(
        (data: PaginatedResponse) => {
          this.extractData(data);
        }
      );
  }

  private getTimesheetsByPageUrl(url: string): void {
    this.timesheetService.getTimesheetsByPageUrl(url)
      .subscribe(
        (data: PaginatedResponse) => {
          this.extractData(data);
        }
      );
  }

  delete(externalId: string): void {
    this.timesheetService.delete(externalId)
      .subscribe(() => this.getTimesheetOnInit());
  }

  private extractData(data: PaginatedResponse): void {
    this.timesheets = data._embedded.timesheets;
    this.links = data._links;
    this.page = data._page;
    if (this.links) {
      this.timesheetService.pageUrl = this.links.self.href;
    }
  }

  selectItems($event: MouseEvent, item: TimesheetOutputModel): void {
    const target = $event.target as HTMLElement;
    if (target.getAttribute('data-selected') === 'false') {
      target.setAttribute('data-selected', 'true');
      target.className = 'far fa-check-square';
      this.selectedItems.set(item.externalId, item);
      this.numberOfSelectedItems.next(this.selectedItems.size);
    } else {
      target.setAttribute('data-selected', 'false');
      target.className = 'far fa-square';
      this.selectedItems.delete(item.externalId);
      this.numberOfSelectedItems.next(this.selectedItems.size);
    }
  }
}
