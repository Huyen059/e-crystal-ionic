import {EmployeeReferenceModel} from '../../../enterprise/employee/_models/EmployeeReferenceModel';

export class TimesheetOutputModel {
  externalId: string;
  month: number;
  status: string;
  year: number;
  totalHours: number;
  totalManDays: number;
  employee: EmployeeReferenceModel;
  name: string;
  numberOfTimesheetEntries: number;
  numberOfTimesheetRemarks: number;
}
