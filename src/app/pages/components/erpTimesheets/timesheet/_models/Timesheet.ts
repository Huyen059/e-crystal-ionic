import {EmployeeOutputModel} from '../../../enterprise/employee/_models/EmployeeOutputModel';

export class Timesheet {
  constructor(
    public externalId: string = null,
    public month: number = null,
    public status: string = null,
    public year: number = null,
    public employee: EmployeeOutputModel = null,
    public name: string = null,
    public pageSize: string = null,
    public sortBy: string = null,
    public isAscending: boolean = null,
  ) {  }
}
