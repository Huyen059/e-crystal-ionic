export class TimesheetPutInputModel {
  constructor(
    public externalId: string = null,
    public month: number = null,
    public status: string = null,
    public year: number = null,
    public employee: string = null,
    public name: string = null,
  ) {  }
}
