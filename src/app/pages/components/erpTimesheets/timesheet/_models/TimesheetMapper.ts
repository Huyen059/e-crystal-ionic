import {TimesheetOutputModel} from './TimesheetOutputModel';
import {Timesheet} from './Timesheet';
import {EmployeeOutputMapper} from '../../../enterprise/employee/_models/EmployeeOutputMapper';

export class TimesheetMapper {
  static map(timesheetOutputModel: TimesheetOutputModel): Timesheet {
    const timesheet = new Timesheet();
    timesheet.externalId = timesheetOutputModel.externalId ? timesheetOutputModel.externalId : null;
    timesheet.month = timesheetOutputModel.month ? timesheetOutputModel.month : null;
    timesheet.status = timesheetOutputModel.status ? timesheetOutputModel.status : null;
    timesheet.year = timesheetOutputModel.year ? timesheetOutputModel.year : null;
    timesheet.employee = timesheetOutputModel.employee ? EmployeeOutputMapper.map(timesheetOutputModel.employee) : null;
    timesheet.name = timesheetOutputModel.name ? timesheetOutputModel.name : null;

    return timesheet;
  }
}
