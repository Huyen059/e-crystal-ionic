import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimesheetWorkspacePage } from './timesheet-workspace.page';
import {TimesheetListComponent} from '../timesheet-list/timesheet-list.component';

const routes: Routes = [
  {
    path: '',
    component: TimesheetWorkspacePage,
    children: [
      {path: '', component: TimesheetListComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimesheetWorkspacePageRoutingModule {}
