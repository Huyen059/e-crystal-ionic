import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimesheetWorkspacePageRoutingModule } from './timesheet-workspace-routing.module';

import { TimesheetWorkspacePage } from './timesheet-workspace.page';
import {UiModule} from '../../../../ui/ui.module';
import {TimesheetListComponent} from '../timesheet-list/timesheet-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UiModule,
    TimesheetWorkspacePageRoutingModule
  ],
  declarations: [
    TimesheetWorkspacePage,
    TimesheetListComponent,
  ]
})
export class TimesheetWorkspacePageModule {}
