import { Component, OnInit } from '@angular/core';
import {MenuController} from '@ionic/angular';

@Component({
  selector: 'app-timesheet-workspace',
  templateUrl: './timesheet-workspace.page.html',
  styleUrls: ['./timesheet-workspace.page.scss'],
})
export class TimesheetWorkspacePage implements OnInit {

  constructor(
      private menu: MenuController
  ) { }

  ngOnInit() {
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

}
