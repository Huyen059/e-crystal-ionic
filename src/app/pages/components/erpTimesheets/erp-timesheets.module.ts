import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErpTimesheetsRoutingModule } from './erp-timesheets-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ErpTimesheetsRoutingModule
  ]
})
export class ErpTimesheetsModule { }
