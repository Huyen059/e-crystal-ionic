export const menuDataDefault = {
  dropdowns: [
    // Dropdown and its items
    {
      title: 'Timesheets',
      items: [
        {name: 'Timesheet', link: '/e-crystal/erpTimesheets/timesheet'},
        {name: 'TimesheetEntry', link: '/e-crystal/erpTimesheets/timesheetEntry'},
        {name: 'TimesheetRemark', link: '/e-crystal/erpTimesheets/timesheetRemark'},
      ],
    },

    // Dropdown and its items
    {
      title: 'Enterprise',
      items: [
        {name: 'Employee', link: '/e-crystal/enterprise/employee'},
      ],
    },
  ],
};
