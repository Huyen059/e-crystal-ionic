export const menuData = {
  dropdowns: [
    // Dropdown and its items
    {
      title: 'Timesheets',
      items: [
        {name: 'Timesheet', link: '/e-crystal/erpTimesheets/timesheet'},
        {name: 'Timesheet Entry', link: '/e-crystal/erpTimesheets/timesheetEntry'},
        {name: 'Timesheet Remark', link: '/e-crystal/erpTimesheets/timesheetRemark'},
      ],
    },

    // Dropdown and its items
    {
      title: 'Enterprise',
      items: [
        {name: 'Employee', link: '/e-crystal/enterprise/employee'},
      ],
    },
  ],
};
