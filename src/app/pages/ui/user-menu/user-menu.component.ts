import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {AuthService} from '../../../login/_services/auth.service';
import {User} from '../../../login/_models/User';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
})
export class UserMenuComponent implements OnInit {

  constructor(
    private router: Router,
    private authService: AuthService,
    private p: PopoverController
  ) { }

  ngOnInit() {}

  get currentUser(): User {
    return this.authService.currentUserValue;
  }

  dismissPopover() {
    this.p.dismiss();
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
