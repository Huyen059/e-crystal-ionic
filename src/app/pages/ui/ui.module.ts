import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MenuComponent} from './menu/menu.component';
import {IonicModule} from '@ionic/angular';
import {PageHeaderComponent} from './page-header/page-header.component';
import {UserMenuComponent} from './user-menu/user-menu.component';
import {RouterModule} from '@angular/router';



@NgModule({
  declarations: [
    MenuComponent,
    PageHeaderComponent,
    UserMenuComponent
  ],
  exports: [
    MenuComponent,
    PageHeaderComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ]
})
export class UiModule { }
