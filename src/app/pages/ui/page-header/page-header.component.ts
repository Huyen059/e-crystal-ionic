import {Component, OnInit} from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {UserMenuComponent} from '../user-menu/user-menu.component';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent implements OnInit {
  popover: any = null;

  constructor(
    private popoverController: PopoverController,
  ) {
  }

  ngOnInit() { }

  async showUserMenu(ev: MouseEvent) {
    this.popover = await this.popoverController.create({
      component: UserMenuComponent,
      event: ev,
      translucent: true
    });

    return this.popover.present();
  }

  dismissPopover() {
    if (this.popover) {
      this.popover.dismiss().then(() => { this.popover = null; });
    }
  }
}
