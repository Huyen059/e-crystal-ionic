import { Injectable } from '@angular/core';
import {authorizationData} from '../../../../credential';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../_models/User';
import {HttpClient} from '@angular/common/http';
import {LoginPostInputModel} from '../_models/LoginPostInputModel';
import {LoginOutputModel} from '../_models/LoginOutputModel';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiBaseUrl = 'http://localhost:9500/e-crystal/api/v1/erpTimesheets' + '/login';
  private httpWriteOptions = {headers: { Authorization : `Basic ${authorizationData}`, 'Content-Type': 'application/json' }};

  currentUserSubject: BehaviorSubject<User>;
  currentUser: Observable<User>;

  constructor(
      private http: HttpClient
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  isAuthenticated(userData: LoginPostInputModel): Observable<boolean> {
    const url = this.apiBaseUrl;
    return this.http.post<LoginOutputModel>(url, userData, this.httpWriteOptions)
        .pipe(
            map(response => {
              if (response.authenticated) {
                const user = new User();
                user.username = userData.username;
                user.password = btoa(userData.password);
                user.authorizationData = btoa(userData.username + ':' + userData.password);
                sessionStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return true;
              } else {
                return false;
              }
            })
        );
  }

  logout(): void {
    sessionStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
