import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from './_services/auth.service';
import {LoginPostInputModel} from './_models/LoginPostInputModel';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  loginError: string;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
      private alertController: AlertController,
  ) {
    if (this.authService.currentUserValue) {
      this.router.navigate(['/e-crystal']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  get f(): { [p: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    const userData = new LoginPostInputModel();
    userData.username = this.f.username.value;
    userData.password = this.f.password.value;

    this.authService.isAuthenticated(userData).subscribe(result => {
      if (result) {
        window.location.replace('/');
      } else {
        this.alertController.create(
          {
            message: 'Username or password is incorrect',
            buttons: ['OK']
          }
        ).then(alertElement => alertElement.present());
      }
    });
  }
}
